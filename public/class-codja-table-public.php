<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Codja_Table
 * @subpackage Codja_Table/public
 */

class Codja_Table_Public
{

  /**
   * The ID of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string $codja_table The ID of this plugin.
   */
  private $codja_table;

  /**
   * The version of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string $version The current version of this plugin.
   */
  private $version;

  /**
   * Initialize the class and set its properties.
   *
   * @param string $plugin_name The name of the plugin.
   * @param string $version The version of this plugin.
   *
   * @since    1.0.0
   */
  public function __construct( $plugin_name, $version )
  {

    $this->codja_table = $plugin_name;
    $this->version = $version;

  }

  /**
   * Register the stylesheets for the public-facing side of the site.
   *
   * @since    1.0.0
   */
  public function enqueue_styles()
  {

    /**
     * This function is provided for demonstration purposes only.
     *
     * An instance of this class should be passed to the run() function
     * defined in Codja_Table_Loader as all of the hooks are defined
     * in that particular class.
     *
     * The Codja_Table_Loader will then create the relationship
     * between the defined hooks and the functions defined in this
     * class.
     */

		wp_enqueue_style( $this->codja_table, plugin_dir_url( __FILE__ ) . 'css/codja-table-public.min.css', array(), $this->version, 'all' );

  }

  /**
   * Register the JavaScript for the public-facing side of the site.
   *
   * @since    1.0.0
   */
  public function enqueue_scripts()
  {

    /**
     * This function is provided for demonstration purposes only.
     *
     * An instance of this class should be passed to the run() function
     * defined in Codja_Table_Loader as all of the hooks are defined
     * in that particular class.
     *
     * The Codja_Table_Loader will then create the relationship
     * between the defined hooks and the functions defined in this
     * class.
     */

		wp_enqueue_script( $this->codja_table, plugin_dir_url( __FILE__ ) . 'js/codja-table-public.min.js', array( 'jquery' ), $this->version, true );

    wp_localize_script( $this->codja_table, 'wp_ajax', array(
      'ajax_url' => admin_url( 'admin-ajax.php' ),
      '_nonce' => wp_create_nonce( 'codja-nonce' ),
    ) );
  }

  /**
   * Preparing roles for which you have users.
   *
   * @return string
   */
  private function get_roles() {
    $all_roles = count_users()['avail_roles'];
    $roles_with_users = [];

    foreach ( $all_roles as $key => $role ) {
      if ( $role ) {
        $roles_with_users[$key] = $role;
      }
    }

    $roles_with_users = array_keys( $roles_with_users );
    $html = '';

    foreach ( $roles_with_users as $role ) {
      $html .= "<option value='$role'>$role</option>";
    }

    return $html;
  }

  /**
   * Calculate the number of pages.
   *
   * @param $paginate int
   * @return float
   */
  private function get_paginate_pages( $paginate ) {
    $count_users = count_users();
    $total_users = $_POST["role"] ?
      $count_users['avail_roles'][$_POST["role"]] :
      $count_users['total_users'];
    return ceil($total_users / $paginate);
  }

  /**
   * Getting the table markup.
   *
   * @return string
   */
  public function add_codja_table()
  {
    if ( ! current_user_can('administrator') ) return;

    return
      '<div id="codja">
          <table>
              <thead>
                  <tr>
                      <th>
                          Name 
                          <button class="js-sort icon-down-dir" data-sort="display_name" type="button"></button>
                      </th>
                      <th>
                          Email
                          <button class="js-sort icon-down-dir" data-sort="email" type="button"></button>
                      </th>
                      <th>
                          <select name="role">
                              <option value="">All roles</option>'
                              . $this->get_roles() .
                          '</select>
                      </th>
                  </tr>
              </thead>
              <tbody></tbody>
          </table>
          <div class="pagination"></div>
      </div>';

  }

  /**
   * Ajax handler. Preparing users data.
   *
   * @return string
   */
  public function ajax_users()
  {

    if ( ! wp_verify_nonce( $_POST['_nonce'], 'codja-nonce' ) ) {
      wp_send_json_error();
      die();
    }

    if ( ! check_ajax_referer('codja-nonce', '_nonce', false) ) {
      wp_send_json_error('Invalid security token sent.');
      die();
    }

    $result = [];
    $paginate = 10;
    $result['pages'] = $this->get_paginate_pages( $paginate );

    $args = [
      'orderby' => $_POST['orderby'] ? sanitize_text_field( $_POST['orderby'] ) : 'display_name',
      'order' => $_POST['order'] ? sanitize_text_field( $_POST['order'] ) : 'ASC',
      'number' => $paginate,
      'paged' => $_POST['page'] ? sanitize_text_field( $_POST['page'] ) : 1,
      'role' => $_POST['role'] ? sanitize_text_field( $_POST['role'] ) : null,
    ];

    $users = get_users( $args );

    foreach ($users as $key => $user) {
      $result['users'][ $key ]['name'] = $user->display_name;
      $result['users'][ $key ]['email'] = $user->user_email;
      $result['users'][ $key ]['role'] = $user->role;
    }

    die( json_encode( $result ) );

  }

}
