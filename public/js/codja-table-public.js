(function ($) {
  'use strict';

  var dataJSON = {
    'action': 'prefix_ajax_first',
    '_nonce': wp_ajax._nonce,
  };

  if ( $('#codja').length > 0  ) {
  	codjaInit();
	}

	// sorting ASC/DESC
	$( 'html' ).on( 'click', '#codja .js-sort', function () {
		var
			$this   = $( this ),
			orderBY = $this.data( 'sort' );

		console.log('CLICK');
		$this.toggleClass( "icon-down-dir icon-up-dir" );
		$this.addClass('active');
		$('#codja .js-sort').not(this).removeClass('active');

		dataJSON.order = $this.hasClass('icon-down-dir') ? 'ASC' : 'DESC';
		dataJSON.orderby = orderBY;
		dataJSON.page = 1;

		sendAjax( dataJSON, function ( res ) {
			renderPagination( res.pages );
		})
	});

	// filtering by roles
	$( 'html' ).on( 'change', 'select[name=role]', function () {
		var
			$this = $( this );
		console.log('jfdk');
		dataJSON.role = $this.val();
		dataJSON.page = 1;

		sendAjax( dataJSON, function ( res ) {
			renderPagination( res.pages );
		})
	});

	// pagination
	$( 'html' ).on( 'click', '#codja .js-paginate', function () {
		var
			$this = $ ( this );

		dataJSON.page = $this.html();

		sendAjax( dataJSON, function ( res ) {
			$this.addClass( 'active' ).siblings().removeClass( 'active' );
		})
	});

	/**
	 * First filling the table and create pagination
	 */
	function codjaInit() {
		sendAjax( dataJSON, function ( res ) {
			renderPagination( res.pages );
		});
	}

	/**
	 * Sending ajax request
	 * @param {object} data данные
	 * @param {function} callback функция обратного вызова
	 */
	function sendAjax( data, callback ) {
		callback = callback || function () {
		};

		$.ajax({
			cache: false,
			type: "POST",
			url: wp_ajax.ajax_url,
			data: dataJSON,
			beforeSend: function( xhr ){
				$( '#codja tbody' ).addClass( 'load' );
			},
			success: function ( response ) {
				var res = JSON.parse( response );
				$( '#codja tbody' ).removeClass( 'load' );
				fillTable( res.users );
				callback( res );
			},
			error: function ( xhr, status, error ) {
				console.log( 'Status: ' + xhr.status );
				console.log( 'Error: ' + xhr.responseText );
			}
		});
	}

	/**
	 * Filling table
	 * @param {object} users
	 */
  function fillTable( users ) {
    var tbody = '';

    for ( var i in users ) {
      tbody += '<tr><td>' +
        users[i].name + '</td>' +
        '<td>' + users[i].email + '</td>' +
        '<td>' + users[i].role + '</td></tr>';
    }

    $( '#codja tbody' ).html( tbody );
  }

	/**
	 * Creating pagination
	 * @param {int} numberOfPages
	 */
  function renderPagination( numberOfPages ) {
    var
      wrapper = $( '#codja .pagination' ),
      pagination_html = '';

    for ( var i = 1; i <= numberOfPages; i++ ) {
      pagination_html += '<button class="js-paginate" type="button">' + i + '</button>';
    }

    wrapper.html( pagination_html );
    wrapper.find( 'button:first-child' ).addClass( 'active' );
  }

})(jQuery);
